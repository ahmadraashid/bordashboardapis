﻿using Dashboard.DataModel.Models;
using Dashboard.Models.Models;
using Dashboard.Models.UnitOfWork;
using Dashboard.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Dashboard.Services
{
    public interface IMutationService
    {
        /// <summary>
        /// Gets list of all mutations
        /// </summary>
        /// <returns></returns>
        IEnumerable<Mutation> GetAll();

        /// <summary>
        /// Gets a Mutation by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Mutation GetById(int id);

        /// <summary>
        /// Get mutations for the provided district
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<Mutation> GetDistrictMutations(int id);

        /// <summary>
        /// Gets top five districts
        /// </summary>
        /// <returns></returns>
        List<Mutation> GetTopFive(DateTime dated);

        /// <summary>
        /// Adds a new Mutation
        /// </summary>
        /// <param name="newMutation"></param>
        /// <returns></returns>
        ActionResponse Add(MutationSummary newMutation);

        /// <summary>
        /// Deletes the provided Mutation
        /// </summary>
        /// <param name="MutationId"></param>
        /// <returns></returns>
        ActionResponse Delete(int MutationId);
    }

    public class MutationService : IMutationService
    {
        private readonly UnitOfWork unitWork;
        IMessageHelper msgHelper;
        ActionResponse response;

        public MutationService()
        {
            unitWork = new UnitOfWork();
            msgHelper = new MessageHelper();
            response = new ActionResponse();
        }

        public IEnumerable<Mutation> GetAll()
        {
            using (var unitWork = new UnitOfWork())
            {
                List<Mutation> mutationsList = new List<Mutation>();
                var mutations = unitWork.MutationRepository.GetAll().ToList();
                if (mutations.Any())
                {
                }
                return null;
            }
        }


        public Mutation GetById(int id)
        {
            using (var unitWork = new UnitOfWork())
            {
                var category = unitWork.MutationRepository.GetByID(id);
                if (category != null)
                {
                }
                return null;
            }
        }

        public List<Mutation> GetTopFive(DateTime dated)
        {
            using (var unitWork = new UnitOfWork())
            {
                List<Mutation> mutationsList = new List<Mutation>();

                var today = DateTime.Now.Date.AddDays(-1);
                var mutations = unitWork.MutationRepository.GetWithInclude(f => f.TimePosted.Year == dated.Year
                    && f.TimePosted.Month == dated.Month
                    && f.TimePosted.Day == dated.Day,
                    new string[] { "District", "Tehsil" });

                var mutationSummary = from mutation in mutations
                                  group mutation by new
                                  {
                                      mutation.District.Id,
                                      mutation.District.Name
                                  }
                                  into g
                                  select new { DistrictId = g.Key.Id, District = g.Key.Name, Mutations = g.ToList().Count, Revenue = g.Sum(r => r.Revenue) };

                foreach (var summary in mutationSummary)
                {
                    mutationsList.Add(new Mutation()
                    {
                        District = summary.District,
                        Mutations = summary.Mutations,
                        Revenue = summary.Revenue
                    });
                }
                return mutationsList;
            }
        }

        public List<Mutation> GetDistrictMutations(int id)
        {
            using (var unitWork = new UnitOfWork())
            {
                List<Mutation> mutationsList = new List<Mutation>();
                var mutations = unitWork.MutationRepository.GetWithInclude(m => m.District.Id.Equals(id));

                foreach(var mutation in mutations)
                {

                }
                return mutationsList;
            }
        }

        public ActionResponse Add(MutationSummary newMutation)
        {
            try
            {
                using (var scope = new TransactionScope())
                {
                    var tehsil = unitWork.TehsilRepository.GetWithInclude(t => t.Id.Equals(newMutation.TehsilId), new string[] { "District" }).FirstOrDefault();
                    if (tehsil != null)
                    {

                        var Mutation = new EFMutation()
                        {
                            District = tehsil.District,
                            Tehsil = tehsil,
                            Mutations = newMutation.Mutations,
                            Revenue = newMutation.Revenue,
                            TimePosted = DateTime.Now
                        };
                        unitWork.MutationRepository.Insert(Mutation);
                        unitWork.Save();
                        scope.Complete();
                        response.Success = true;
                        response.ReturnedId = Mutation.Id;
                    }

                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }



        public ActionResponse Delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetCannotBeZeroMessage("Mutation Id");
                    return response;
                }

                var mutation = unitWork.MutationRepository.Get(f => f.Id.Equals(id));
                if (mutation != null)
                {
                    unitWork.MutationRepository.Delete(mutation);
                    unitWork.Save();
                    response.Success = true;
                    response.ReturnedId = id;
                }
                else
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFoundMessage("Mutation");
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
