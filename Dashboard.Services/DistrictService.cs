﻿using Dashboard.Models.Models;
using Dashboard.Models.UnitOfWork;
using Dashboard.Services.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace Dashboard.Services
{
    public interface IDistrictService
    {
        /// <summary>
        /// Gets all districts async
        /// </summary>
        /// <returns></returns>
        IEnumerable<District> GetAll();
    }

    public class DistrictService : IDistrictService
    {
        private readonly UnitOfWork unitWork;
        IMessageHelper msgHelper;
        ActionResponse response;

        public DistrictService()
        {
            unitWork = new UnitOfWork();
            msgHelper = new MessageHelper();
            response = new ActionResponse();
        }

        public IEnumerable<District> GetAll()
        {
            using (var unitWork = new UnitOfWork())
            {
                List<District> districtsList = new List<District>();
                var districts = unitWork.DistrictRepository.GetAll();
                if (districts.Any())
                {
                    foreach(var district in districts)
                    {
                        districtsList.Add(new District()
                        {
                            Id = district.Id,
                            Name = district.Name
                        });
                    }
                    return districtsList;
                }
                else
                    return null;
            }
        }
    }
}
