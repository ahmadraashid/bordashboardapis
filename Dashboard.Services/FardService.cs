﻿using Dashboard.DataModel.Models;
using Dashboard.Models.Models;
using Dashboard.Models.UnitOfWork;
using Dashboard.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Dashboard.Services
{
    public interface IFardService
    {
        /// <summary>
        /// Gets list of all fards
        /// </summary>
        /// <returns></returns>
        IEnumerable<Fard> GetAll();

        /// <summary>
        /// Gets a Fard by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Fard GetById(int id);

        /// <summary>
        /// Gets top five districts
        /// </summary>
        /// <returns></returns>
        List<Fard> GetTopFive(DateTime dated);

        /// <summary>
        /// Adds a new Fard
        /// </summary>
        /// <param name="newFard"></param>
        /// <returns></returns>
        ActionResponse Add(FardSummary newFard);

        /// <summary>
        /// Deletes the provided Fard
        /// </summary>
        /// <param name="FardId"></param>
        /// <returns></returns>
        ActionResponse Delete(int FardId);
    }

    public class FardService : IFardService
    {
        private readonly UnitOfWork unitWork;
        IMessageHelper msgHelper;
        ActionResponse response;

        public FardService()
        {
            unitWork = new UnitOfWork();
            msgHelper = new MessageHelper();
            response = new ActionResponse();
        }

        public IEnumerable<Fard> GetAll()
        {
            using (var unitWork = new UnitOfWork())
            {
                List<Fard> fardsList = new List<Fard>();
                var fards = unitWork.FardRepository.GetAll().ToList();
                if (fards.Any())
                {
                }
                return null;
            }
        }


        public Fard GetById(int id)
        {
            using (var unitWork = new UnitOfWork())
            {
                var category = unitWork.FardRepository.GetByID(id);
                if (category != null)
                {
                }
                return null;
            }
        }

        public List<Fard> GetTopFive(DateTime dated)
        {
            using (var unitWork = new UnitOfWork())
            {
                List<Fard> fardsList = new List<Fard>();
                
                var today = DateTime.Now.Date.AddDays(-1);
                var fards = unitWork.FardRepository.GetWithInclude(f => f.TimePosted.Year == dated.Year 
                    && f.TimePosted.Month == dated.Month
                    && f.TimePosted.Day == dated.Day, 
                    new string[] { "District", "Tehsil" });


                var fardSummary = from fard in fards
                                  group fard by new
                                  {
                                      fard.District.Id,
                                      fard.District.Name
                                  }
                                  into g
                                  select new { DistrictId = g.Key.Id , District = g.Key.Name, Fards = g.ToList().Count, Revenue = g.Sum(r => r.Revenue) };
                
                //if (fardSummary.Any())
                //{
                    foreach(var summary in fardSummary)
                    {
                        fardsList.Add(new Fard()
                            {
                                District = summary.District,
                                Fards = summary.Fards,
                                Revenue = summary.Revenue
                            });
                  }
                    return fardsList;
                //}
            }
        }

        public ActionResponse Add(FardSummary newFard)
        {
            try
            {
                using (var scope = new TransactionScope())
                {
                    var tehsil = unitWork.TehsilRepository.GetWithInclude(t => t.Id.Equals(newFard.TehsilId), new string[] { "District" }).FirstOrDefault();
                    if (tehsil != null)
                    {
                        
                        var Fard = new EFFard()
                        {
                            District = tehsil.District,
                            Tehsil = tehsil,
                            Fards = newFard.Fards,
                            Revenue = newFard.Revenue,
                            TimePosted = DateTime.Now
                        };
                        unitWork.FardRepository.Insert(Fard);
                        unitWork.Save();
                        scope.Complete();
                        response.Success = true;
                        response.ReturnedId = Fard.Id;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        

        public ActionResponse Delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetCannotBeZeroMessage("Fard Id");
                    return response;
                }

                var fard = unitWork.FardRepository.Get(f => f.Id.Equals(id));
                if (fard != null)
                {
                    unitWork.FardRepository.Delete(fard);
                    unitWork.Save();
                    response.Success = true;
                    response.ReturnedId = id;
                }
                else
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFoundMessage("Fard");
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }

}
