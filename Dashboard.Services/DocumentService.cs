﻿using Dashboard.DataModel.Models;
using Dashboard.Models.Models;
using Dashboard.Models.UnitOfWork;
using Dashboard.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace Dashboard.Services
{
    public interface IDocumentService
    {
        /// <summary>
        /// Gets list of all documentSummarys
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentSummary> GetAll();

        /// <summary>
        /// Gets all document types
        /// </summary>
        /// <returns></returns>
        IEnumerable<DocumentTypes> GetDocumentTypes();

        /// <summary>
        /// Gets top five districts
        /// </summary>
        /// <returns></returns>
        DistrictDocumentSummary GetDistrictTehsilsSummary(DateFilter filter);

        /// <summary>
        /// Adds a new Document
        /// </summary>
        /// <param name="newDocument"></param>
        /// <returns></returns>
        ActionResponse Add(NewDocument newDocument);

        /// <summary>
        /// Deletes the provided Document
        /// </summary>
        /// <param name="DocumentId"></param>
        /// <returns></returns>
        ActionResponse Delete(int DocumentId);
    }

    public class DocumentService : IDocumentService
    {
        private readonly UnitOfWork unitWork;
        IMessageHelper msgHelper;
        ActionResponse response;

        public DocumentService()
        {
            unitWork = new UnitOfWork();
            msgHelper = new MessageHelper();
            response = new ActionResponse();
        }

        public IEnumerable<DocumentSummary> GetAll()
        {
            using (var unitWork = new UnitOfWork())
            {
                var documentSummarys = unitWork.DocumentRepository.GetAll();
                if (documentSummarys.Any())
                {
                }
                return null;
            }
        }
        
        public IEnumerable<DocumentTypes> GetDocumentTypes()
        {
            using (var unitWork = new UnitOfWork())
            {
                List<DocumentTypes> docTypes = new List<DocumentTypes>();
                var documentTypes = unitWork.DocumentTypeRepository.GetAll();
                if (documentTypes.Any())
                {
                    foreach(var docType in documentTypes)
                    {
                        docTypes.Add(new DocumentTypes()
                        {
                            Id = docType.Id,
                            Title = docType.Title,
                            Abbrevation = docType.Abbrevation
                        });
                    }
                }
                return docTypes;
            }
        }

        public ActionResponse Add(NewDocument newDocument)
        {

            using (var scope = new TransactionScope())
            {
                try
                {

                    var tehsil = unitWork.TehsilRepository.GetWithInclude(t => t.Id.Equals(newDocument.TehsilId), new string[] { "District" }).FirstOrDefault();
                    var documentType = unitWork.DocumentTypeRepository.GetByID(newDocument.DocumentTypeId);

                    if (tehsil != null && documentType != null)
                    {
                        var documentSummary = new EFDocuments()
                        {
                            District = tehsil.District,
                            Tehsil = tehsil,
                            DocumentType = documentType,
                            Count = newDocument.Count,
                            TimePosted = DateTime.Now
                        };

                        var docDetailObj = unitWork.DocumentDetailRepository.GetByID(newDocument.FileId);
                        EFDocumentsDetail documentDetail = null;

                        if (docDetailObj == null)
                        {
                            documentDetail = new EFDocumentsDetail()
                            {
                                Id = newDocument.FileId,
                                District = tehsil.District,
                                Tehsil = tehsil,
                                DocumentType = documentType,
                                CurrentStatus = (EFDocumentsDetail.DocumentStatus)newDocument.Status,
                                Dated = DateTime.Now
                            };
                            unitWork.DocumentRepository.Insert(documentSummary);
                            unitWork.DocumentDetailRepository.Insert(documentDetail);
                        }
                        else
                        {
                            docDetailObj.CurrentStatus = (EFDocumentsDetail.DocumentStatus)newDocument.Status;
                            docDetailObj.Dated = DateTime.Now;
                            unitWork.DocumentDetailRepository.Update(docDetailObj);
                        }

                        unitWork.Save();
                        scope.Complete();
                        response.Success = true;
                        response.ReturnedId = documentSummary.Id;
                    }
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = ex.Message;
                }
                return response;
            }


        }

        public DistrictDocumentSummary GetDistrictTehsilsSummary(DateFilter filter)
        {
            int districtId = filter.Id;
            DistrictDocumentSummary documentSummary = new DistrictDocumentSummary();
            DateTime dated = Convert.ToDateTime(filter.Dated);
            using (var unitWork = new UnitOfWork())
            {
                documentSummary.Id = districtId;
                var documents = unitWork.DocumentRepository
                    .GetWithInclude(t => t.District.Id.Equals(districtId) && 
                    dated.Year == t.TimePosted.Year && dated.Month == t.TimePosted.Month &&
                    dated.Day == t.TimePosted.Day, new string[] { "District" });

                var documentDetails = unitWork.DocumentDetailRepository
                    .GetWithInclude(t => t.District.Id.Equals(districtId) &&
                    dated.Year == t.Dated.Year && dated.Month == t.Dated.Month &&
                    dated.Day == t.Dated.Day, new string[] { "District" });

                //Tehsil wise summary of documents
                var docsSummaryList = from document in documents
                                       group document by new
                                       {
                                           TehsildId = document.Tehsil.Id,
                                           TehsilName = document.Tehsil.Name,
                                           DocumentTypeId = document.DocumentType.Id,
                                           DocumentTitle = document.DocumentType.Title
                                       }
                                  into g
                                       select new
                                       {
                                           TehsilId = g.Key.TehsildId,
                                           TehsilName = g.Key.TehsilName,
                                           DocumentTypeId = g.Key.DocumentTypeId,
                                           DocumentTitle = g.Key.DocumentTitle,
                                           Count = g.ToList().Count
                                       };

                //Tehsil wise summary of document statuses
                var docTypesSummaryList = from docType in documentDetails
                                          group docType by new
                                          {
                                              TehsilId = docType.Tehsil.Id,
                                              DocumentTypeId = docType.DocumentType.Id,
                                              Status = docType.CurrentStatus,
                                          } 
                                          into grp
                                          select new
                                          {
                                              TehsilId = grp.Key.TehsilId,
                                              DocumentTypeId = grp.Key.DocumentTypeId,
                                              Status = grp.Key.Status,
                                              Count = grp.ToList().Count
                                          };

                /*var docStatusDistSummary = from docType in docTypesSummaryList
                                       group docType by new
                                       {
                                           Status = docType.Status
                                       }
                                       into g
                                       select new
                                       {
                                           Status = g.Key.Status,
                                           Count = g.ToList().Count
                                       };*/

                List<DocumentStatusSummary> districtDocStatus = new List<DocumentStatusSummary>();
                foreach(var docType in docTypesSummaryList)
                {
                    if (districtDocStatus.Count == 0)
                    {
                        districtDocStatus.Add(new DocumentStatusSummary()
                        {
                            Status = (DocumentStatusSummary.DocumentStatus)docType.Status,
                            Count = docType.Count
                        });
                    }
                    else
                    {
                        var obj = districtDocStatus.Find(d => d.Status == (DocumentStatusSummary.DocumentStatus)docType.Status);
                        if (obj != null)
                        {
                            obj.Count += docType.Count;
                        }
                        else
                        {
                            districtDocStatus.Add(new DocumentStatusSummary()
                            {
                                Status = (DocumentStatusSummary.DocumentStatus)docType.Status,
                                Count = docType.Count
                            });
                        }
                    }
                }

                documentSummary.DocumentSummary = new List<DocumentSummary>();
                documentSummary.DocumentStatusSummary = new List<DocumentStatusSummary>();

                foreach(var docStatusSummary in districtDocStatus)
                {
                    documentSummary.DocumentStatusSummary.Add(new DocumentStatusSummary()
                    {
                        Status = (DocumentStatusSummary.DocumentStatus)docStatusSummary.Status,
                        Count = docStatusSummary.Count
                    });
                }

                foreach(var summary in docsSummaryList)
                {
                    var docTypeSummary = from docSummary in docTypesSummaryList
                                         where docSummary.TehsilId.Equals(summary.TehsilId) &&
                                         docSummary.DocumentTypeId.Equals(summary.DocumentTypeId)
                                         select docSummary;
                    List<DocumentStatusSummary> docStatusSummary = new List<DocumentStatusSummary>();
                    
                    if (docTypeSummary != null)
                    {
                        foreach (var docTSummary in docTypeSummary)
                        {
                            docStatusSummary.Add(new DocumentStatusSummary()
                            {
                                Status = (DocumentStatusSummary.DocumentStatus)docTSummary.Status,
                                Count = docTSummary.Count
                            });
                        }
                    }

                    documentSummary.DocumentSummary.Add(new DocumentSummary()
                    {
                        TehsilId = summary.TehsilId,
                        Tehsil = summary.TehsilName,
                        DocumentTitle = summary.DocumentTitle,
                        Count = summary.Count,
                        DocStatusSummaries = docStatusSummary
                    });
                }
                return documentSummary;
            }
        }

        public ActionResponse Delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    response.Success = false;
                    response.Message = msgHelper.GetCannotBeZeroMessage("Document Id");
                    return response;
                }

                var documentSummary = unitWork.DocumentRepository.Get(f => f.Id.Equals(id));
                if (documentSummary != null)
                {
                    unitWork.DocumentRepository.Delete(documentSummary);
                    unitWork.Save();
                    response.Success = true;
                    response.ReturnedId = id;
                }
                else
                {
                    response.Success = false;
                    response.Message = msgHelper.GetNotFoundMessage("Document");
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
