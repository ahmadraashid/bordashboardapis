﻿using Dashboard.Models.Models;
using Dashboard.Models.UnitOfWork;
using Dashboard.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Dashboard.Services
{
    public interface ITehsilService
    {
        /// <summary>
        /// Gets all tehsils
        /// </summary>
        /// <returns></returns>
        IEnumerable<Tehsil> GetAll();

        /// <summary>
        /// Gets tehsils for the provided districtId
        /// </summary>
        /// <param name="districtId"></param>
        /// <returns></returns>
        DistrictSummary GetDistrictTehsilsSummary(DateFilter filter);
    }

    public class TehsilService : ITehsilService
    {
        private readonly UnitOfWork unitWork;
        IMessageHelper msgHelper;
        ActionResponse response;

        public TehsilService()
        {
            unitWork = new UnitOfWork();
            msgHelper = new MessageHelper();
            response = new ActionResponse();
        }

        public IEnumerable<Tehsil> GetAll()
        {
            using (var unitWork = new UnitOfWork())
            {
                List<Tehsil> tehsilsList = new List<Tehsil>();
                var tehsils = unitWork.TehsilRepository.GetWithInclude(t => t.Id != 0, new string[] { "District" });
                if (tehsils.Any())
                {
                    foreach (var tehsil in tehsils)
                    {
                        tehsilsList.Add(new Tehsil()
                        {
                            Id = tehsil.Id,
                            DistrictId = tehsil.District.Id,
                            Name = tehsil.Name
                        });
                    }
                    return tehsilsList;
                }
                else
                    return null;
            }
        }

        public DistrictSummary GetDistrictTehsilsSummary (DateFilter filter)
        {
            int districtId = filter.Id;
            using (var unitWork = new UnitOfWork())
            {
                DistrictSummary districtSummary = new DistrictSummary();
                districtSummary.Id = districtId;
                var dated = Convert.ToDateTime(filter.Dated);
                var tehsils = unitWork.TehsilRepository.GetWithInclude(t => t.District.Id.Equals(districtId), new string[] { "District" });
                var tehsilIds = (from tehsil in tehsils
                                 select tehsil.Id).ToList<int>();

                var fards = unitWork.FardRepository.GetWithInclude(f => tehsilIds.Contains(f.Tehsil.Id) &&
                    f.TimePosted.Year == dated.Year && f.TimePosted.Month == dated.Month &&
                    f.TimePosted.Day == dated.Day, new string[] { "Tehsil" });

                var mutations = unitWork.MutationRepository.GetWithInclude(m => tehsilIds.Contains(m.Tehsil.Id) &&
                    m.TimePosted.Year == dated.Year && m.TimePosted.Month == dated.Month &&
                    m.TimePosted.Day == dated.Day, new string[] { "Tehsil", "District" });

                var fardSummary = from fard in fards
                                  group fard by new
                                  {
                                      fard.Tehsil.Id
                                  }
                                  into g
                                  select new { TehsilId = g.Key.Id, Fards = g.ToList().Count, Revenue = g.Sum(r => r.Revenue) };


                var mutationSummary = from mutation in mutations
                                      group mutation by new
                                      {
                                          mutation.Tehsil.Id
                                      }
                                      into g
                                      select new { TehsilId = g.Key.Id, Mutations = g.ToList().Count, Revenue = g.Sum(r => r.Revenue) };

                if (tehsils.Any())
                {
                    int fardsCount = 0, mutationsCount = 0;
                    decimal fardsRevenue = 0, mutationsRevenue = 0;

                    foreach (var tehsil in tehsils)
                    {
                        fardsCount = 0; mutationsCount = 0;
                        fardsRevenue = 0; mutationsRevenue = 0;

                        var fardObj = (from fard in fardSummary
                                      where fard.TehsilId.Equals(tehsil.Id)
                                      select fard).FirstOrDefault();

                        var mutationObj = (from mutation in mutationSummary
                                           where mutation.TehsilId.Equals(tehsil.Id)
                                           select mutation).FirstOrDefault();

                        if (fardObj != null)
                        {
                            fardsCount = fardObj.Fards;
                            fardsRevenue = fardObj.Revenue;
                        }

                        if (mutationObj != null)
                        {
                            mutationsCount = mutationObj.Mutations;
                            mutationsRevenue = mutationObj.Revenue;
                        }

                        if (districtSummary.RevenueSummary == null)
                            districtSummary.RevenueSummary = new List<RevenueSummary>();

                        districtSummary.RevenueSummary.Add(new RevenueSummary()
                        {
                            TehsilId = tehsil.Id,
                            Tehsil = tehsil.Name,
                            FardsCount = fardsCount,
                            FardRevenue = fardsRevenue,
                            MutationsCount = mutationsCount,
                            MutationRevenue = mutationsRevenue
                        });

                        fardObj = null;
                        mutationObj = null;

                    }
                    return districtSummary;
                }
                else
                    return null;
            }
        }
    }
}
