﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.DataModel.Models
{
    public class EFDistrict
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<EFTehsil> Tehsils { get; set; }
        public ICollection<EFFard> Fards { get; set; }
    }

    public class EFTehsil
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public EFDistrict District { get; set; }
        public string Name { get; set; }
    }

    public class EFDocumentTypes
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Abbrevation { get; set; }
        public ICollection<EFDocuments> Documents { get; set; }
    }

    public class EFDocuments
    {
        public int Id { get; set; }
        public EFDocumentTypes DocumentType { get; set; }
        public EFDistrict District { get; set; }
        public EFTehsil Tehsil { get; set; }
        public int Count { get; set; }
        public DateTime TimePosted { get; set; }
    }

    public class EFDocumentsDetail
    {
        public enum DocumentStatus
        {
            Received = 1,
            InProcess = 2,
            Pending = 3,
            Completed = 4
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public EFDistrict District { get; set; }
        public EFTehsil Tehsil { get; set; }
        public EFDocumentTypes DocumentType { get; set; }
        public DocumentStatus CurrentStatus { get; set; }
        public DateTime Dated { get; set; }
    }

    public class EFFard
    {
        public int Id { get; set; }
        public EFDistrict District { get; set; }
        public EFTehsil Tehsil { get; set; }
        public int Fards { get; set; }
        public decimal Revenue { get; set; }
        public DateTime TimePosted { get; set; }
    }

    public class EFMutation
    {
        public int Id { get; set; }
        public EFDistrict District { get; set; }
        public EFTehsil Tehsil { get; set; }
        public int Mutations { get; set; }
        public decimal Revenue { get; set; }
        public DateTime TimePosted { get; set; }
    }
}
