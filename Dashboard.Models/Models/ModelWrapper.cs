﻿
using System.Collections.Generic;

namespace Dashboard.Models.Models
{

    public class ActionResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int? ReturnedId { get; set; }
    }

    public class Fard
    {
        public string District { get; set; }
        public int Fards { get; set; }
        public decimal Revenue { get; set; }
    }

    public class District
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Tehsil
    {
        public int Id { get; set; }
        public int DistrictId { get; set; }
        public string Name { get; set; }
    }

    public class DistrictSummary
    {
        public int Id { get; set; }
        public List<RevenueSummary> RevenueSummary { get; set; }
    }

    public class DistrictDocumentSummary
    {
        public int Id { get; set; }
        public List<DocumentSummary> DocumentSummary { get; set; }
        public List<DocumentStatusSummary> DocumentStatusSummary { get; set; }
    }

    public class RevenueSummary
    {
        public int TehsilId { get; set; }
        public string Tehsil { get; set; }
        public int FardsCount { get; set; }
        public decimal FardRevenue { get; set; }
        public int MutationsCount { get; set; }
        public decimal MutationRevenue { get; set; }
    }

    public class Mutation
    {
        public string District { get; set; }
        public int Mutations { get; set; }
        public decimal Revenue { get; set; }
    }

    public class TehsilsDetailSummary
    {
        public int TehsilId { get; set; }
        public int Fards { get; set; }
        public decimal FardsRevenue { get; set; }
        public int Mutations { get; set; }
        public decimal MutationsRevenue { get; set; }
    }

    public class FilterData
    {
        public string Dated { get; set; }
        public string Time { get; set; }
        public bool IsTimeIncluded { get; set; }
    }

    public class DateFilter
    {
        public int Id { get; set; }
        public string Dated { get; set; }
    }

    public class FardSummary
    {
        public int TehsilId { get; set; }
        public int Fards { get; set; }
        public decimal Revenue { get; set; }
    }

    public class MutationSummary
    {
        public int TehsilId { get; set; }
        public int Mutations { get; set; }
        public decimal Revenue { get; set; }
    }

    public class DocumentSummary
    {
        public int TehsilId { get; set; }
        public string Tehsil { get; set; }
        public string DocumentTitle { get; set; }
        public int Count { get; set; }
        public List<DocumentStatusSummary> DocStatusSummaries { get; set; }
    }

    public class DocumentStatusSummary
    {
        public enum DocumentStatus
        {
            Received = 1,
            InProcess = 2,
            Pending = 3,
            Completed = 4
        }
        public DocumentStatus Status { get; set; }
        public int Count { get; set; }
    }

    public class NewDocument
    {
        public enum DocumentStatus
        {
            Received = 1,
            InProcess = 2,
            Pending = 3,
            Completed = 4
        }

        public int FileId { get; set; }
        public int DistrictId { get; set; }
        public int TehsilId { get; set; }
        public int DocumentTypeId { get; set; }
        public int Count { get; set; }
        public DocumentStatus Status { get; set; }
    }

    public class DocumentTypes
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Abbrevation { get; set; }
    }
}
