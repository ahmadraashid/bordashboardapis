namespace Dashboard.DataModel.Migrations
{
    using Dashboard.DataModel.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Dashboard.DataModel.DashboardDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Dashboard.DataModel.DashboardDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            if (context.DocumentTypes.Count() == 0)
            {
                context.DocumentTypes.Add(new EFDocumentTypes() { Id = 11, Title = "Register Haqdaran-e-Zameen", Abbrevation = "RHZ" });
                context.DocumentTypes.Add(new EFDocumentTypes() { Id = 12, Title = "Register Intqalat", Abbrevation = "MUT" });
                context.DocumentTypes.Add(new EFDocumentTypes() { Id = 13, Title = "Fard-e-Badar", Abbrevation = "FRB" });
                context.DocumentTypes.Add(new EFDocumentTypes() { Id = 14, Title = "Register Gardawari", Abbrevation = "GRD" });
                context.SaveChanges();
            }

            if (context.Districts.Count() == 0)
            {
                var peshawar = context.Districts.Add(new EFDistrict() { Id = 3, Name = "Peshawar" });
                var charsadda = context.Districts.Add(new EFDistrict() { Id = 1, Name = "Charsadda" });
                var nowshera = context.Districts.Add(new EFDistrict() { Id = 2, Name = "Nowshera" });
                var bannu = context.Districts.Add(new EFDistrict() { Id = 17, Name = "Bannu" });
                var mardan = context.Districts.Add(new EFDistrict() { Id = 18, Name = "Mardan" });
                var abbotabad = context.Districts.Add(new EFDistrict() { Id = 21, Name = "Abbotabad" });
                var kohat = context.Districts.Add(new EFDistrict() { Id = 22, Name = "Kohat" });
                var buner = context.Districts.Add(new EFDistrict() { Id = 23, Name = "Buner" });
                var deraIsmailKhan = context.Districts.Add(new EFDistrict() { Id = 24, Name = "Dera Ismail Khan" });

                context.SaveChanges();

                //Peshawar Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 15, District = peshawar, Name = "Peshawar" });

                //Charsadda Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 11, District = charsadda, Name = "Charsadda" });
                context.Tehsils.Add(new EFTehsil() { Id = 12, District = charsadda, Name = "Tangi" });
                context.Tehsils.Add(new EFTehsil() { Id = 13, District = charsadda, Name = "Shabqadar" });

                //Nowshera Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 14, District = nowshera, Name = "Pabbi" });
                context.Tehsils.Add(new EFTehsil() { Id = 24, District = nowshera, Name = "Nowshera" });
                context.Tehsils.Add(new EFTehsil() { Id = 25, District = nowshera, Name = "Kherabad" });

                //Bannu Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 16, District = bannu, Name = "Bannu" });

                //Mardan Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 19, District = mardan, Name = "Mardan" });
                context.Tehsils.Add(new EFTehsil() { Id = 20, District = mardan, Name = "Takht Bhai" });
                context.Tehsils.Add(new EFTehsil() { Id = 21, District = mardan, Name = "Katlang" });

                //Abbotabad Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 22, District = abbotabad, Name = "Abbotabad" });
                context.Tehsils.Add(new EFTehsil() { Id = 23, District = abbotabad, Name = "Havelian" });

                //Kohat Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 17, District = kohat, Name = "Kohat" });
                context.Tehsils.Add(new EFTehsil() { Id = 18, District = kohat, Name = "Lachi" });

                //Buner Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 26, District = buner, Name = "Daggar" });
                context.Tehsils.Add(new EFTehsil() { Id = 27, District = buner, Name = "Sawari" });
                context.Tehsils.Add(new EFTehsil() { Id = 28, District = buner, Name = "Khadokhel" });
                context.Tehsils.Add(new EFTehsil() { Id = 29, District = buner, Name = "Gagra" });
                context.Tehsils.Add(new EFTehsil() { Id = 30, District = buner, Name = "Mandanr" });

                //DI Khan Tehsils
                context.Tehsils.Add(new EFTehsil() { Id = 31, District = deraIsmailKhan, Name = "Dera Ismail Khan" });
                context.Tehsils.Add(new EFTehsil() { Id = 32, District = deraIsmailKhan, Name = "Kulachi" });
                context.Tehsils.Add(new EFTehsil() { Id = 33, District = deraIsmailKhan, Name = "Darabun" });
                context.Tehsils.Add(new EFTehsil() { Id = 34, District = deraIsmailKhan, Name = "Pharpur" });
                context.Tehsils.Add(new EFTehsil() { Id = 35, District = deraIsmailKhan, Name = "Paroa" });

                try
                {
                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    if (ex.InnerException != null)
                    {
                        var message  = ex.InnerException.Message;
                    }
                }
                
            }
            


        }
    }
}
