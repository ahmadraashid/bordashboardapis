namespace Dashboard.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_Migration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EFDistricts",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EFFards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fards = c.Int(nullable: false),
                        Revenue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TimePosted = c.DateTime(nullable: false),
                        District_Id = c.Int(),
                        Tehsil_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EFDistricts", t => t.District_Id)
                .ForeignKey("dbo.EFTehsils", t => t.Tehsil_Id)
                .Index(t => t.District_Id)
                .Index(t => t.Tehsil_Id);
            
            CreateTable(
                "dbo.EFTehsils",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        District_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EFDistricts", t => t.District_Id)
                .Index(t => t.District_Id);
            
            CreateTable(
                "dbo.EFDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Count = c.Int(nullable: false),
                        TimePosted = c.DateTime(nullable: false),
                        District_Id = c.Int(),
                        DocumentType_Id = c.Int(),
                        Tehsil_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EFDistricts", t => t.District_Id)
                .ForeignKey("dbo.EFDocumentTypes", t => t.DocumentType_Id)
                .ForeignKey("dbo.EFTehsils", t => t.Tehsil_Id)
                .Index(t => t.District_Id)
                .Index(t => t.DocumentType_Id)
                .Index(t => t.Tehsil_Id);
            
            CreateTable(
                "dbo.EFDocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Title = c.String(),
                        Abbrevation = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EFMutations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Mutations = c.Int(nullable: false),
                        Revenue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TimePosted = c.DateTime(nullable: false),
                        District_Id = c.Int(),
                        Tehsil_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EFDistricts", t => t.District_Id)
                .ForeignKey("dbo.EFTehsils", t => t.Tehsil_Id)
                .Index(t => t.District_Id)
                .Index(t => t.Tehsil_Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.EFMutations", "Tehsil_Id", "dbo.EFTehsils");
            DropForeignKey("dbo.EFMutations", "District_Id", "dbo.EFDistricts");
            DropForeignKey("dbo.EFDocuments", "Tehsil_Id", "dbo.EFTehsils");
            DropForeignKey("dbo.EFDocuments", "DocumentType_Id", "dbo.EFDocumentTypes");
            DropForeignKey("dbo.EFDocuments", "District_Id", "dbo.EFDistricts");
            DropForeignKey("dbo.EFFards", "Tehsil_Id", "dbo.EFTehsils");
            DropForeignKey("dbo.EFTehsils", "District_Id", "dbo.EFDistricts");
            DropForeignKey("dbo.EFFards", "District_Id", "dbo.EFDistricts");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.EFMutations", new[] { "Tehsil_Id" });
            DropIndex("dbo.EFMutations", new[] { "District_Id" });
            DropIndex("dbo.EFDocuments", new[] { "Tehsil_Id" });
            DropIndex("dbo.EFDocuments", new[] { "DocumentType_Id" });
            DropIndex("dbo.EFDocuments", new[] { "District_Id" });
            DropIndex("dbo.EFTehsils", new[] { "District_Id" });
            DropIndex("dbo.EFFards", new[] { "Tehsil_Id" });
            DropIndex("dbo.EFFards", new[] { "District_Id" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.EFMutations");
            DropTable("dbo.EFDocumentTypes");
            DropTable("dbo.EFDocuments");
            DropTable("dbo.EFTehsils");
            DropTable("dbo.EFFards");
            DropTable("dbo.EFDistricts");
        }
    }
}
