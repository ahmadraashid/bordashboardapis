namespace Dashboard.DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DocumentDetails_Table_Added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EFDocumentsDetails",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        CurrentStatus = c.Int(nullable: false),
                        Dated = c.DateTime(nullable: false),
                        District_Id = c.Int(),
                        DocumentType_Id = c.Int(),
                        Tehsil_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EFDistricts", t => t.District_Id)
                .ForeignKey("dbo.EFDocumentTypes", t => t.DocumentType_Id)
                .ForeignKey("dbo.EFTehsils", t => t.Tehsil_Id)
                .Index(t => t.District_Id)
                .Index(t => t.DocumentType_Id)
                .Index(t => t.Tehsil_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EFDocumentsDetails", "Tehsil_Id", "dbo.EFTehsils");
            DropForeignKey("dbo.EFDocumentsDetails", "DocumentType_Id", "dbo.EFDocumentTypes");
            DropForeignKey("dbo.EFDocumentsDetails", "District_Id", "dbo.EFDistricts");
            DropIndex("dbo.EFDocumentsDetails", new[] { "Tehsil_Id" });
            DropIndex("dbo.EFDocumentsDetails", new[] { "DocumentType_Id" });
            DropIndex("dbo.EFDocumentsDetails", new[] { "District_Id" });
            DropTable("dbo.EFDocumentsDetails");
        }
    }
}
