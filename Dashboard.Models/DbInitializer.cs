﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.DataModel
{
    class DropCreateDatabaseAlways : DropCreateDatabaseAlways<DashboardDBContext>
        {
        public override void InitializeDatabase(DashboardDBContext context)
            {
            base.InitializeDatabase(context);
            }

        protected override void Seed(DashboardDBContext context)
            {
            DataSeeder.Seed(context);
            base.Seed(context);
            }
        }
    class CreateDatabaseIfNotExists : CreateDatabaseIfNotExists<DashboardDBContext>
        {
        public override void InitializeDatabase(DashboardDBContext context)
            {
            base.InitializeDatabase(context);
            }

        protected override void Seed(DashboardDBContext context)
            {
            DataSeeder.Seed(context);
            base.Seed(context);
            }
        }

    class MigrateDatabaseToLatestVersion : MigrateDatabaseToLatestVersion<DashboardDBContext, Migrations.Configuration>
        {
        public override void InitializeDatabase(DashboardDBContext context)
            {
            base.InitializeDatabase(context);
            }
        }

    class NoInitializer : IDatabaseInitializer<DashboardDBContext>
        {

        public void InitializeDatabase(DashboardDBContext context)
            {

            }
        }

    class DataSeeder
        {

        public static void Seed(DashboardDBContext context)
            {
            //Initialize any Pre-requisite data
            }
        }
}
