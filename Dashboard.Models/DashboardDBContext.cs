﻿using Dashboard.DataModel.Models;
using System.Data.Entity;

namespace Dashboard.DataModel
    {
    public class DashboardDBContext : ApplicationDbContext
        {
        
        public DashboardDBContext(IDatabaseInitializer<DashboardDBContext> dbInitializer)
            : base()
            {
            if (dbInitializer != null)
                Database.SetInitializer(dbInitializer);
            }

        public DashboardDBContext()
            : this(new MigrateDatabaseToLatestVersion())
            {
            }

        public DbSet<EFFard> Fards { get; set; }
        public DbSet<EFMutation> Mutations { get; set; }
        public DbSet<EFDistrict> Districts { get; set; }
        public DbSet<EFTehsil> Tehsils { get; set; }
        public DbSet<EFDocumentTypes> DocumentTypes { get; set; }
        public DbSet<EFDocuments> Documents { get; set; }
        public DbSet<EFDocumentsDetail> DocumentsDetail { get; set; }
        }
    }
