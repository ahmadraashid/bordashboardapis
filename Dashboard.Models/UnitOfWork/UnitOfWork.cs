﻿using Dashboard.DataModel;
using Dashboard.DataModel.Models;
using Dashboard.Models.Models;
using Dashboard.Models.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.UnitOfWork
{
    public class UnitOfWork : IDisposable
    {
        private DashboardDBContext context = null;
        private GenericRepository<EFDistrict> districtRepository;
        private GenericRepository<EFTehsil> tehsilRepository;
        private GenericRepository<EFFard> fardRepository;
        private GenericRepository<EFMutation> mutationRepository;
        private GenericRepository<Fard> fardCustomRepository;
        private GenericRepository<EFDocumentTypes> documentTypeRepository;
        private GenericRepository<EFDocuments> documentRepository;
        private GenericRepository<EFDocumentsDetail> documentDetailRepository;

        public UnitOfWork()
        {
            context = new DashboardDBContext();
        }

        public GenericRepository<EFFard> FardRepository
        {
            get
            {
                if (this.fardRepository == null)
                    this.fardRepository = new GenericRepository<EFFard>(context);
                return fardRepository;
            }
        }

        public GenericRepository<EFMutation> MutationRepository
        {
            get
            {
                if (this.mutationRepository == null)
                    this.mutationRepository = new GenericRepository<EFMutation>(context);
                return mutationRepository;
            }
        }

        public GenericRepository<Fard> FardCustomRepository
        {
            get
            {
                if (this.fardCustomRepository == null)
                    this.fardCustomRepository = new GenericRepository<Fard>(context);
                return fardCustomRepository;
            }
        }

        public GenericRepository<EFDistrict> DistrictRepository
        {
            get
            {
                if (this.districtRepository == null)
                    this.districtRepository = new GenericRepository<EFDistrict>(context);
                return districtRepository;
            }
        }

        public GenericRepository<EFTehsil> TehsilRepository
        {
            get
            {
                if (this.tehsilRepository == null)
                    this.tehsilRepository = new GenericRepository<EFTehsil>(context);
                return tehsilRepository;
            }
        }

        public GenericRepository<EFDocumentTypes> DocumentTypeRepository
        {
            get
            {
                if (this.documentTypeRepository == null)
                    this.documentTypeRepository = new GenericRepository<EFDocumentTypes>(context);
                return documentTypeRepository;
            }
        }

        public GenericRepository<EFDocuments> DocumentRepository
        {
            get
            {
                if (this.documentRepository == null)
                    this.documentRepository = new GenericRepository<EFDocuments>(context);
                return documentRepository;
            }
        }

        public GenericRepository<EFDocumentsDetail> DocumentDetailRepository
        {
            get
            {
                if (this.documentDetailRepository == null)
                    this.documentDetailRepository = new GenericRepository<EFDocumentsDetail>(context);
                return documentDetailRepository;
            }
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"E:\errors.txt", outputLines);
                throw e;
            }
        }

        private bool disposed = false;
        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
