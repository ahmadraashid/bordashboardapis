﻿using Dashboard.APIs.Helpers;
using Dashboard.APIs.SignalR;
using Dashboard.Models.Models;
using Dashboard.Services;
using Microsoft.AspNet.SignalR;
using System;
using System.Net;
using System.Web.Http;

namespace Dashboard.APIs.Controllers
{
    [RoutePrefix("api/fard")]
    public class FardController : ApiController
    {
        private IFardService fardService;
        /// <summary>
        /// Public constructor to initialize Fard service instance
        /// </summary>
        public FardController()
        {
        }

        public IHttpActionResult Get()
        {
            fardService = new FardService();
            var fards = fardService.GetAll();
            if (fards != null)
            {
                return Ok(fards);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Fards"));
        }

        [Route("loadTopFive")]
        public IHttpActionResult LoadTopFive([FromBody] FilterData data)
        {
            string dateStr = "";
            if (data.IsTimeIncluded)
                dateStr = data.Dated + " " + data.Time;
            else
                dateStr = data.Dated;

            DateTime dated;
            bool validDate = DateTime.TryParse(dateStr, out dated);
            if (!validDate)
            {
                return Content(HttpStatusCode.BadRequest, "Invalid Date Time provided");
            }
            fardService = new FardService();
            var fards = fardService.GetTopFive(dated);
            if (fards != null)
            {
                return Ok(fards);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Fards"));
        }

        // GET api/Fard/5
        public IHttpActionResult Get(int id)
        {
            fardService = new FardService();
            var Fard = fardService.GetById(id);
            if (Fard != null)
                return Ok(Fard);

            return Content(HttpStatusCode.NotFound, APIMessageHelper.EntityNotFoundMessage("Fard", id));
        }

        // POST api/Fard
        public IHttpActionResult Post([FromBody] FardSummary fard)
        {
            fardService = new FardService();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = fardService.Add(fard);
            if (response.Success)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<Notifier>();
                hubContext.Clients.All.newFard(response.ReturnedId);
                return Ok(response.ReturnedId);
            }
            return Content(HttpStatusCode.BadRequest, response.Message);
        }

        // PUT api/Fard/5
        public IHttpActionResult Put(int id, [FromBody]Fard fard)
        {
            return Ok("No Implemented");
        }

        // DELETE api/Fard/5
        public IHttpActionResult Delete(int id)
        {
            fardService = new FardService();
            var response = fardService.Delete(id);
            if (response.Success)
                return Ok(id);

            return Content(HttpStatusCode.BadRequest, response.Message);
        }
    }
}
