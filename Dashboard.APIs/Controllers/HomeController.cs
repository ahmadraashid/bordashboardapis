﻿using Dashboard.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dashboard.APIs.Controllers
    {
    public class HomeController : Controller
        {
        DashboardDBContext db;

        public HomeController()
        {
            db = new DashboardDBContext();
            db.Database.Initialize(false);
        }

        public ActionResult Index()
            {
            ViewBag.Title = "Home Page";

            return View();
            }
        }
    }
