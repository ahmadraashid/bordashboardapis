﻿using Dashboard.APIs.Helpers;
using Dashboard.APIs.SignalR;
using Dashboard.Models.Models;
using Dashboard.Services;
using Microsoft.AspNet.SignalR;
using System;
using System.Net;
using System.Web.Http;

namespace Dashboard.APIs.Controllers
{
    [RoutePrefix("api/Document")]
    public class DocumentController : ApiController
    {
        private IDocumentService documentService;
        /// <summary>
        /// Public constructor to initialize Document service instance
        /// </summary>
        public DocumentController()
        {
        }

        public IHttpActionResult Get()
        {
            documentService = new DocumentService();
            var documents = documentService.GetAll();
            if (documents != null)
            {
                return Ok(documents);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Documents"));
        }

        [Route("getDocumentTypes")]
        public IHttpActionResult GetDocumentTypes()
        {
            documentService = new DocumentService();
            var docTypes = documentService.GetDocumentTypes();
            if (docTypes != null)
            {
                return Ok(docTypes);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Document Types"));
        }

        [Route("getDistrictDocumentsSummary")]
        public IHttpActionResult DistrictTehsilSummary([FromBody] DateFilter data)
        {
            string dateStr = data.Dated;
            DateTime dated;
            bool validDate = DateTime.TryParse(dateStr, out dated);
            if (!validDate)
            {
                return Content(HttpStatusCode.BadRequest, "Invalid Date Time provided");
            }
            documentService = new DocumentService();
            var documents = documentService.GetDistrictTehsilsSummary(data);
            if (documents != null)
            {
                return Ok(documents);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Documents"));
        }

        // POST api/Document
        public IHttpActionResult Post([FromBody] NewDocument document)
        {
            documentService = new DocumentService();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = documentService.Add(document);
            if (response.Success)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<Notifier>();
                hubContext.Clients.All.newDocument(document.DistrictId);
                return Ok(response.ReturnedId);
            }
            return Content(HttpStatusCode.BadRequest, response.Message);
        }

        // PUT api/Document/5
        public IHttpActionResult Put(int id, [FromBody]NewDocument document)
        {
            return Ok("No Implemented");
        }

        // DELETE api/Document/5
        public IHttpActionResult Delete(int id)
        {
            documentService = new DocumentService();
            var response = documentService.Delete(id);
            if (response.Success)
                return Ok(id);

            return Content(HttpStatusCode.BadRequest, response.Message);
        }
    }
}
