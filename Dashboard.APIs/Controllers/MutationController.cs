﻿using Dashboard.APIs.Helpers;
using Dashboard.APIs.SignalR;
using Dashboard.Models.Models;
using Dashboard.Services;
using Microsoft.AspNet.SignalR;
using System;
using System.Net;
using System.Web.Http;

namespace Dashboard.APIs.Controllers
{
    [RoutePrefix("api/mutation")]
    public class MutationController : ApiController
    {
        private IMutationService mutationService;
        /// <summary>
        /// Public constructor to initialize Mutation service instance
        /// </summary>
        public MutationController()
        {
        }

        public IHttpActionResult Get()
        {
            mutationService = new MutationService();
            var mutations = mutationService.GetAll();
            if (mutations != null)
            {
                return Ok(mutations);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Mutations"));
        }

        [Route("loadTopFive")]
        public IHttpActionResult LoadTopFive([FromBody] FilterData data)
        {
            string dateStr = "";
            if (data.IsTimeIncluded)
                dateStr = data.Dated + " " + data.Time;
            else
                dateStr = data.Dated;

            DateTime dated;
            bool validDate = DateTime.TryParse(dateStr, out dated);
            if (!validDate)
            {
                return Content(HttpStatusCode.BadRequest, "Invalid Date Time provided");
            }
            mutationService = new MutationService();
            var mutations = mutationService.GetTopFive(dated);
            if (mutations != null)
            {
                return Ok(mutations);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Mutations"));
        }

        // GET api/Mutation/5
        public IHttpActionResult Get(int id)
        {
            mutationService = new MutationService();
            var Mutation = mutationService.GetById(id);
            if (Mutation != null)
                return Ok(Mutation);

            return Content(HttpStatusCode.NotFound, APIMessageHelper.EntityNotFoundMessage("Mutation", id));
        }

        // POST api/Mutation
        public IHttpActionResult Post([FromBody] MutationSummary mutation)
        {
            mutationService = new MutationService();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = mutationService.Add(mutation);
            if (response.Success)
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<Notifier>();
                hubContext.Clients.All.newMutation(response.ReturnedId);
                return Ok(response.ReturnedId);
            }
            return Content(HttpStatusCode.BadRequest, response.Message);
        }

        // PUT api/Mutation/5
        public IHttpActionResult Put(int id, [FromBody]Mutation mutation)
        {
            return Ok("No Implemented");
        }

        // DELETE api/Mutation/5
        public IHttpActionResult Delete(int id)
        {
            mutationService = new MutationService();
            var response = mutationService.Delete(id);
            if (response.Success)
                return Ok(id);

            return Content(HttpStatusCode.BadRequest, response.Message);
        }
    }
}