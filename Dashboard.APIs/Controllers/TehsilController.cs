﻿using Dashboard.APIs.Helpers;
using Dashboard.Models.Models;
using Dashboard.Services;
using System.Net;
using System.Web.Http;

namespace Dashboard.APIs.Controllers
{
    [RoutePrefix("api/tehsil")]
    public class TehsilController : ApiController
    {
        private ITehsilService tehsilService;
        /// <summary>
        /// Public constructor to initialize Tehsil service instance
        /// </summary>
        public TehsilController()
        {
        }


        public IHttpActionResult Get()
        {
            tehsilService = new TehsilService();
            var tehsils = tehsilService.GetAll();
            if (tehsils != null)
            {
                return Ok(tehsils);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Tehsils"));
        }

        [HttpPost]
        [Route("districtTehsilsSummary")]
        public IHttpActionResult DistrictTehsilsSummary([FromBody]DateFilter filter)
        {
            tehsilService = new TehsilService();
            var tehsils = tehsilService.GetDistrictTehsilsSummary(filter);
            if (tehsils != null)
            {
                return Ok(tehsils);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Tehsils"));
        }
    }
}
