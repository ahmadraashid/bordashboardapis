﻿using Dashboard.APIs.Helpers;
using Dashboard.Services;
using System.Net;
using System.Web.Http;

namespace Dashboard.APIs.Controllers
{
    [RoutePrefix("district")]
    public class DistrictController : ApiController
    {
        private IDistrictService districtService;
        /// <summary>
        /// Public constructor to initialize District service instance
        /// </summary>
        public DistrictController()
        {
        }


        public IHttpActionResult Get()
        {
            districtService = new DistrictService();
            var districts = districtService.GetAll();
            if (districts != null)
            {
                return Ok(districts);
            }
            return Content(HttpStatusCode.NotFound, APIMessageHelper.ListNotFoundMessage("Districts"));
        }
    }
}
