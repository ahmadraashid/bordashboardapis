﻿using System.Web;
using System.Web.Mvc;

namespace Dashboard.APIs
    {
    public class FilterConfig
        {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
            {
            filters.Add(new HandleErrorAttribute());
            }
        }
    }
