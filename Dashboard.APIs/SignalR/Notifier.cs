﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard.APIs.SignalR
    {
    public class Notifier : Hub
        {
        public void NotifyFard(int code)
            {
            Clients.All.newFard(code);
            }
        }
    }